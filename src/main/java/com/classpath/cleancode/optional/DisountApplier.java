package com.classpath.cleancode.optional;

import java.util.Optional;

public class DisountApplier {

    private static final DisountApplier discountApplier = new DisountApplier();

    public static DisountApplier getInstance(){
       return discountApplier;
    }

    public String applyDiscount(Product product){
        Optional<Integer> discountInPercentage = getDiscountPercentage(product.getPrice());

        discountInPercentage
                .ifPresent(discount -> System.out.println("Discount is "+ discount));


        return discountInPercentage.isPresent() ? discountInPercentage.get() + "%" : "";
         //return discountInPercentage != null ? discountInPercentage+" %": "";
    }

    private Optional<Integer> getDiscountPercentage(double productPrice) {
        if(productPrice >= 5000){
            return Optional.of(15);
        } else if(productPrice >= 2000) {
            return Optional.of(5);
        }
        return Optional.empty();
    }
}