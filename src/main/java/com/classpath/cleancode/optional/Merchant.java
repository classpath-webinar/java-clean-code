package com.classpath.cleancode.optional;

public class Merchant {
    public static void main(String[] args) {
        Product iPhone = new Product(12, "IPhone", 75_000);
        Product samsung = new Product(13, "Samsung", 15_000);
        Product vivo = new Product(13, "Vivo", 4000);
        Product memoryCard  = new Product(14, "SD-Card-50GB", 400);

        //never create instance using new operator
        DisountApplier applier = DisountApplier.getInstance();

        String discountForIPhone  = applier.applyDiscount(iPhone);
        String discountForSamsung  = applier.applyDiscount(samsung);
        String discountForVivo  = applier.applyDiscount(vivo);
        String discountForMemoryCard = applier.applyDiscount(memoryCard);

        System.out.println(discountForIPhone);
        System.out.println(discountForSamsung);
        System.out.println(discountForVivo);
        System.out.println(discountForMemoryCard);
    }
}