package com.classpath.cleancode.enums;

public enum Movie {
    REGULAR(4),
    NEW_RELEASE(5),
    CHILDREN(2);

    private final int rating;

    Movie(int rating){
        System.out.println("Came inside the constructor.."+rating);
        this.rating = rating;
    }
}