package com.classpath.cleancode.strategy;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.*;

//Convert from List to Set
public class FilterEmployeeImpl implements FilterEmployee  {

    //demonstrate the declarative approach
    public List<Employee> filterEmployeeByName(final String firstname) {
        Set<Employee> employeesSet = EmployeeUtil.fetchEmployees();

        //imperative code
        /*List<Employee> list = new ArrayList<>();
        for (Employee e : employeesSet) {
            if (e.getFirstName().equalsIgnoreCase(firstname)) {
                list.add(e);
            }
        }*/
        return employeesSet
                .stream()
                .filter(employee -> employee.getFirstName().equalsIgnoreCase(firstname))
                .collect(Collectors.toList());

        //return list;
    }

    @Override
    public List<Employee> filterEmployeesByName(String firstname) {
        return null;
    }

    @Override
    public List<Employee> filterEmployeesByDeptName(String deptName) {
        return null;
    }

    @Override
    public List<Employee> filterEmployeesBySalary(double salary) {
        return null;
    }

    //demonstrate the generic
    public List<Employee> applyFilter(Predicate<Employee> predicate){
        Set<Employee> employeesSet = EmployeeUtil.fetchEmployees();
        return employeesSet
                .stream()
                .filter(predicate)
                .collect(Collectors.toList());
    }
}