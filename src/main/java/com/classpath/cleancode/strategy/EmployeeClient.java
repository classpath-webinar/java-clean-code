package com.classpath.cleancode.strategy;

import java.util.List;

public class EmployeeClient {
    public static void main(String[] args) {
        FilterEmployee filterEmployee = new FilterEmployeeImpl();
       // List<Employee> employees = ((FilterEmployeeImpl) filterEmployee).filterEmployeeByName("roopa");
       // System.out.println(employees);

        // improved version
        //extract the name and print
        filterEmployee
                //55_000 is a magic number. can be extract into a separate method.
                .applyFilter(employee -> (employee.getSalary() > 55_000 && employee.getSalary() < 75_000))
                .forEach(System.out::println);
    }
}