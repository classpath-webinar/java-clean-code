package com.classpath.cleancode.strategy;

import lombok.NonNull;

import java.io.Serializable;
import java.util.Objects;

//Demonstrate the setId, equals and hashcode and implement Serializable and Comparable interfaces
//Demonstrate the Lombok library
public class Employee implements Serializable, Comparable<Employee> {

    private long empId;
    private String firstName;
    private String lastName;
    private String deptName;

    private double salary;

    public Employee(long empId, String firstName, String lastName, String deptName, double salary) {
        this.empId = empId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.deptName = deptName;
        this.salary = salary;
    }

    public Employee(long empId, String firstName, String lastName) {
        this.empId = empId;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public long getEmpId() {
        return empId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Employee employee = (Employee) o;
        return empId == employee.empId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(empId);
    }

    @Override
    public String toString() {
        return "Employee{" +
                "empId=" + empId +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", deptName='" + deptName + '\'' +
                ", salary=" + salary +
                '}';
    }

    @Override
    public int compareTo(Employee employee) {
        return (int) (this.empId - employee.getEmpId());
    }
}