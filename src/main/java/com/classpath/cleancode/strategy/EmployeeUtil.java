package com.classpath.cleancode.strategy;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class EmployeeUtil {

    public static final Set<Employee> fetchEmployees(){
        List<Employee> employees = Arrays.asList(
                new Employee(12, "Dheeraj", "Mehra", "HR", 45_000),
                new Employee(16, "Harish", "Krishna", "Payroll", 75_000),
                new Employee(17, "Vikram", "Patil", "HR", 85_000),
                new Employee(22, "Joseph", "Meer", "FINANCE", 65_000),
                new Employee(52, "Roopa", "Patel", "HR", 55_000)
        );
        return new HashSet<>(employees);
    }
}