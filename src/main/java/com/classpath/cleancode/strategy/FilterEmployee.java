package com.classpath.cleancode.strategy;

import java.util.List;
import java.util.function.Predicate;

public interface FilterEmployee {

    List<Employee> filterEmployeesByName(String firstname);

    List<Employee> filterEmployeesByDeptName(String deptName);

    List<Employee> filterEmployeesBySalary(double salary);

    List<Employee> applyFilter(Predicate<Employee> predicate);
}