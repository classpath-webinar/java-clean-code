package com.classpath.cleancode.types.general;

public class Movie {

    enum Type {
        REGULAR, NEW_RELEASE, CHILDREN
    }

    private final Type type;

    public Movie(Type type) {
        this.type = type;
    }

    public int computePrice(int days) {
        switch (type) {

            case REGULAR: return days * 20;

            case NEW_RELEASE: return days * 40;

            case CHILDREN: return 50;

            default: throw new IllegalArgumentException();
        }
    }
}