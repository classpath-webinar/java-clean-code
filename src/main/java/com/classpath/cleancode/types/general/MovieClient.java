package com.classpath.cleancode.types.general;

import static com.classpath.cleancode.types.general.Movie.Type.*;
import static com.classpath.cleancode.types.general.Movie.Type.NEW_RELEASE;

public class MovieClient {
    public static void main(String[] args) {
        Movie titanic = new Movie(REGULAR);
        Movie ipMan4 = new Movie(NEW_RELEASE);
        Movie kingKong = new Movie(CHILDREN);

        System.out.printf("Price for Titanic for %d days : %s Rupees %n", 2, titanic.computePrice(2));
        System.out.printf("Price for IpMan-4 for %d days  : %s Rupees %n", 2, ipMan4.computePrice(2));
        System.out.printf("Price for King Kong for %d days : %s Rupees %n", 2, kingKong.computePrice(2));
    }
}