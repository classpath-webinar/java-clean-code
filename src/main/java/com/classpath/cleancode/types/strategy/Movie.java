package com.classpath.cleancode.types.strategy;

public class Movie {

    enum Type {
        REGULAR, NEW_RELEASE, CHILDREN
    }
    private final Type type;

    public Movie(Type type) {
        this.type = type;
    }
}