package com.classpath.cleancode.types.strategy;

public class PriceService {

    public int computeNewReleasePrice(int days){return (days * 40);}

    public int computeRegularPrice(int days) {
        return days * 20;
    }

    public int computeChildrenPrice(int days) {
        return 50;
    }

    public int computePrice(Movie.Type type, int days) {
        switch (type) {
            case REGULAR: return computeRegularPrice(days);
            case NEW_RELEASE: return computeNewReleasePrice(days);
            case CHILDREN: return computeChildrenPrice(days);
            default: throw new IllegalArgumentException();
        }
    }
}