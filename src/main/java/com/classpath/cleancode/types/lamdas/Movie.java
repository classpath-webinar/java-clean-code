package com.classpath.cleancode.types.lamdas;

import java.util.function.BiFunction;

public class Movie {

    public enum Type {

        REGULAR(PricingService::computeRegularPrice),
        NEW_RELEASE(PricingService::computeNewReleasePrice),
        CHILDREN(PricingService::computeChildrenPrice),
        ACTION(PricingService::computeRegularPrice);

        Type(BiFunction<PricingService, Integer, Integer> priceAlgo) {
            this.priceAlgo = priceAlgo;
        }

        public final BiFunction<PricingService, Integer, Integer> priceAlgo;
    }
}