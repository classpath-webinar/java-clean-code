package com.classpath.cleancode.types.lamdas;

public class MovieClient {

    public static void main(String[] args) {
        PricingService priceService = new PricingService(new PricingRepository());
        System.out.printf("Price for Regular movie %d %n", priceService.computePrice(Movie.Type.REGULAR, 2));
        System.out.printf("Price for New Released movie %d %n", priceService.computePrice(Movie.Type.NEW_RELEASE, 2));
        System.out.printf("Price for Children movie %d %n", priceService.computePrice(Movie.Type.CHILDREN, 2));
    }
}