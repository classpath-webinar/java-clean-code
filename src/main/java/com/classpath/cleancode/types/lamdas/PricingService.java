package com.classpath.cleancode.types.lamdas;

//@Repository
class PricingRepository {
    public int getFactor(){
        return 4;
    }
}

public class PricingService {

    private final PricingRepository repo;

    //@Autowired
    public PricingService(PricingRepository repository){
        this.repo = repository;
    }

    public int computeNewReleasePrice(int days) {
        //return days * repository.getFactor();
        return (days * 40);
    }

    public int computeRegularPrice(int days) { return days * 20; }

    public int computeChildrenPrice(int days) {
        return 50;
    }

    public int computePrice(Movie.Type type, int days) {
        return type.priceAlgo.apply(this, days);
    }
}