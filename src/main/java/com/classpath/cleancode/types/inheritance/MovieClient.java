package com.classpath.cleancode.types.inheritance;


public class MovieClient {
    public static void main(String[] args) {
        Movie titanic = new RegularMovie();
        Movie ipMan4 = new NewReleaseMovie();
        Movie kingKong = new ChildrenMovie();

        System.out.printf("Price for Titanic for %d days : %s Rupees %n", 2, titanic.computePrice(2));
        System.out.printf("Price for IpMan-4 for %d days  : %s Rupees %n", 2, ipMan4.computePrice(2));
        System.out.printf("Price for King Kong for %d days : %s Rupees %n", 2, kingKong.computePrice(2));
    }
}