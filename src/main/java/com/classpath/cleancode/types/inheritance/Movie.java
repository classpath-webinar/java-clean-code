package com.classpath.cleancode.types.inheritance;

abstract class Movie {
    public abstract int computePrice(int days);
}

class RegularMovie extends Movie {
    public int computePrice(int days) {
        return days * 20;
    }
}

class NewReleaseMovie extends Movie {
    public int computePrice(int days) {
        return days * 40;
    }
}

class ChildrenMovie extends Movie {
    public int computePrice(int days) {
        return 50;
    }
}