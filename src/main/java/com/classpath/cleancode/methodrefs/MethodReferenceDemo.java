package com.classpath.cleancode.methodrefs;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import static java.util.stream.Collectors.toList;

public class MethodReferenceDemo {
    public static void main(String[] args) {
        UserFacade facade = new UserFacade();
        facade.getAllUsers().forEach((user)-> System.out.println(user));
    }
}

class UserFacade {

    private UserRepo userRepo = new UserRepo();

    public List<UserDto> getAllUsers() {
        return userRepo.findAll().stream().map(UserMapper::toDto).collect(toList());
    }
}

class UserMapper {

    public static UserDto toDto(User user) {
        UserDto dto = new UserDto();
        dto.setFullName(user.getFirstName() + " " + user.getLastName().toUpperCase());
        return dto;
    }
}

@Data
class UserDto {

/*public UserDto(User user) {
    this.setFullName(user.getFirstName() + " " + user.getLastName().toUpperCase());
}
*/
    private String fullName;
}

@Data
@Builder
class User {
    private String firstName;
    private String lastName;
}

class UserRepo {
    public List<User> findAll(){
        return Arrays.asList(
                User.builder().firstName("Vinay").lastName("Kumar").build(),
                User.builder().firstName("Krishna").lastName("Hari").build(),
                User.builder().firstName("Debi").lastName("Prasad").build(),
                User.builder().firstName("Akshaya").lastName("Patil").build(),
                User.builder().firstName("Harish").lastName("Pai").build()
        );
    }
}