package com.classpath.cleancode.cohesion;

//Not a cohesive class
public class Calculator {
    //Not a cohesive code..
    public int sum(int operand1, int operand2){
        return operand1 + operand2;
    }

    public static void main(String[] args) {
        Calculator calculator = new Calculator();
        int result = calculator.sum(23, 34);
        System.out.println("Print the result "+ result);
    }
}