package com.classpath.cleancode.junit;

import com.classpath.cleancode.optional.Product;

import java.util.List;

public class ProductService {

    private final ProductRepository productRepo;

    public ProductService(ProductRepository productRepository){
        this.productRepo = productRepository;
    }

    public List<Product> fetchAll(){
        return this.productRepo.fetchAll();
    }

    public Product findById(long id){
        return this.productRepo
                .findById(id)
                //.orElse(()-> new Product());
                .orElseThrow(ProductService::invalidProduct);
    }

    private static IllegalArgumentException invalidProduct() {
        return new IllegalArgumentException("No matching product found");
    }

    public void deleteProduct(long productId){
        Product product = this.productRepo
                .findById(productId)
                //.orElse(()-> new Product());
                .orElseThrow(ProductService::invalidProduct);
        if (product != null){
            //delete the product
            this.productRepo.deleteProduct(product);
        }
    }
}