package com.classpath.cleancode.junit;

import com.classpath.cleancode.optional.Product;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class ProductRepository {

    private List<Product> productList = Arrays.asList(
            new Product(12, "IPhone", 75_000),
            new Product(13, "Samsung", 15_000),
            new Product(13, "Vivo", 4000),
            new Product(14, "SD-Card-50GB", 400)
    );

    public List<Product> fetchAll(){
        return productList;
    }

    public Optional<Product> findById(long id){
        return productList
                .stream()
                .filter(product -> product.getProductId() == id).findFirst();
    }

    public void deleteProduct(Product product) {
       productList
                .removeIf(p -> p.getProductId() == product.getProductId() );
    }
}