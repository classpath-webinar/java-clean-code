package com.classpath.cleancode.junit;

import java.util.Random;

public class LoadDisbursement {

    public double disburseLoan(long customerId, double loanAmountReq){
        return performVerification(customerId)? loanAmountReq: 0;
    }

    protected static boolean performVerification(long customerId) {

        int randomNumber = new Random().nextInt(7);
        return (Math.floorMod(randomNumber, 2) == 1) ? true : false;
    }
}