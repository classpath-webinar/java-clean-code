package com.classpath.junit;

import com.classpath.cleancode.junit.ProductRepository;
import com.classpath.cleancode.junit.ProductService;
import com.classpath.cleancode.optional.Product;
import mockit.*;
import org.junit.Test;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import static org.junit.Assert.*;

public class ProductServiceTest {

  @Mocked
  private ProductRepository mockProductRepo;

  @Tested(fullyInitialized = true)
  private ProductService productService;

  @Test
  public void testFetchAllProducts() {
      new Expectations()
      {{
          mockProductRepo.fetchAll();
          result = Arrays.asList(
                  new Product(1, "TV", 34_000),
                  new Product(2, "IPhone", 54_000),
                  new Product(3, "Thinkpad-T580", 1_34_000));
          times = 1;
      }};

      List<Product> products = productService.fetchAll();

      //assertion
      assertEquals(3, products.size());

      //verification
      new Verifications(){{mockProductRepo.fetchAll(); times = 1;}};
  }

  @Test
  public void testFindValidProductById() {
      //expectations
      new Expectations()
      {{
          mockProductRepo.findById(anyLong);
          result = new Product(1, "Samsung-TV", 34_000);
      }};

      Product product = productService.findById(12);

      //assertion
      assertNotNull(product);
      assertEquals(product.getPrice(), 34_000,0);
      assertEquals(product.getName(), "Samsung-TV");

      //verification
      new Verifications(){{mockProductRepo.findById(12); times = 1;}};
  }

    @Test
    public void testFindProductByValidIdWithNoException() {
        //expectations
        new Expectations()
        {{
            mockProductRepo.findById(anyLong);
            result = new Product(1, "Samsung-TV", 34_000);
        }};
        try {
            Product product = productService.findById(12);

            assertNotNull(product);
        }catch (Exception exception){
            fail("Should not throw exception");
        }
        //verification
        new Verifications(){{mockProductRepo.findById(12); times = 1;}};

    }

    @Test
    public void testFindProductByInvalidIdHandleException() {
        //expectations
        new Expectations()
        {{
            mockProductRepo.findById(anyLong);
            result = new IllegalArgumentException("Invalid Product Id");
        }};

        try {
            Product product = productService.findById(12);
            fail("Should not come in this block");
        }catch (Exception exception){
            assertTrue(exception instanceof IllegalArgumentException);
        }
        //verification
        new Verifications(){{mockProductRepo.findById(12); times = 1;}};
    }

    @Test(expected = IllegalArgumentException.class)
    public void testFindProductByInvalidId() {
      //expectations
      new Expectations()
      {{
          mockProductRepo.findById(anyLong);
          result = new IllegalArgumentException("Invalid Product Id");
      }};

       Product product = productService.findById(12);
      //verification
        new Verifications(){{mockProductRepo.findById(12); times = 1;}};
    }

    @Test(expected = IllegalArgumentException.class)
    public void testDeleteProduct(){
        //expectations
        new Expectations()
        {{
            mockProductRepo.findById(anyLong); result = Optional.empty(); times = 1;
            mockProductRepo.deleteProduct((Product) any); times = 0;
        }};

        productService.deleteProduct(12);
        //verification
        //will never be called.
       // new Verifications(){{
         //   mockProductRepo.findById(12); times = 12;}};
    }

    @Test
    public void testDeleteProductValid(){
        //expectations
        new Expectations()
        {{
            mockProductRepo.findById(anyLong); result = new Product(12, "IPhone", 20_000); times = 1;
            mockProductRepo.deleteProduct((Product) any); times = 1;
        }};

        productService.deleteProduct(12);
        //verification

        new Verifications(){{
            mockProductRepo.findById(12); times = 1;
            mockProductRepo.deleteProduct((Product)any); times = 1;
        }};
    }
}