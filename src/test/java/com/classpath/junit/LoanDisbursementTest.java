package com.classpath.junit;

import com.classpath.cleancode.junit.LoadDisbursement;
import com.classpath.cleancode.junit.ProductService;
import mockit.Mock;
import mockit.MockUp;
import mockit.Tested;
import org.junit.Assert;
import org.junit.Test;

public class LoanDisbursementTest {

    @Tested
    private LoadDisbursement loadDisbursement;
    @Test
    public void testValidCustomerIdWithGoodCreditScore() {

        new MockUp<LoadDisbursement>(){
            @Mock
            boolean performVerification(long customerId){
                return true;
            }
        };

        double loanAmount = loadDisbursement.disburseLoan(1234, 20_000);
        Assert.assertEquals(20_000, loanAmount, 0.0);
    }

    @Test
    public void testValidCustomerIdWithBadCreditScore() {

        new MockUp<LoadDisbursement>(){
            @Mock
            boolean performVerification(long customerId){
                return false;
            }
        };

        double loanAmount = loadDisbursement.disburseLoan(1234, 20_000);
        Assert.assertEquals(0.0, loanAmount, 0.0);
    }
}