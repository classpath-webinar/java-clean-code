package com.classpath.junit;

import com.classpath.cleancode.optional.Product;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class ProductTest {

    private static Product product;

    @BeforeClass
    public static void init(){
        product = new Product(12, "IPhone", 55_999);
    }

    @Test
    public void testConstructor(){
        assertEquals(product.getProductId(), 12);
        assertEquals(product.getName(), "Samsung-TV");
        assertEquals(product.getPrice(), 55_999, 0.0);
    }

    @Before
    public void setup(){
        product.setPrice(55_999);
        product.setName("Samsung-TV");
    }

    @Test
    public void testSetProductId(){
        assertEquals(product.getProductId(), 12);
        assertEquals(product.getName(), "Samsung-TV");
        assertEquals(product.getPrice(), 55_999, 0.0);
    }

    @Test
    public void testEquals(){
        Product product1 = new Product(12, "IPhone", 55_999);
        Product product2 = new Product(12, "IPhone-X", 55_999);
        assertEquals(product1, product2);
    }

    @Test
    public void testHashCode(){
        Product product1 = new Product(12, "IPhone", 55_999);
        Product product2 = new Product(12, "IPhone-X", 55_999);
        assertEquals(product1.hashCode(), product2.hashCode());
    }

    @Test
    public void testNotHashCode(){
        Product product1 = new Product(13, "IPhone", 55_999);
        Product product2 = new Product(12, "IPhone-X", 55_999);
        assertNotEquals(product1.hashCode(), product2.hashCode());
    }
}