# Clean Code
Welcome to Clean Code session using Java Language 

## Java Examples 
All the examples demonstrated uses Java 8

## Cohesion
- Strive for high cohesive code
- Should have "Single Responsibility"
- Check for Code smell with a "but" test
- Easy to test, maintain and refactor

## Strategy
- Use strategy design pattern to pass behaviour (Behavior parameterization)
- Avoid using if-else style of programming - Code smell and Poor code quality
- Avoid excessing switch block - Code smell and Poor code
- Use platform provided Functional Interfaces for most part
- Use method references as opposed to anonymous inner classes
- Strive for immutable code with no side affects
- Build composable code with Functional style constructs

## Optional
- Avoid surprises with "Null"
- Always alarm clients with Optional when the result may be null
- Avoid using `isPresent` and use `ifPresent` constructs
- Perform stream operations to process data
- Return empty List instead of sending null

## Design patterns to solve types 
 - Using Enums with `Switch` case - Rudimentary
 - Using Abstract classes - Provides structural enforcement but is inflexible
 - Using Strategy design patterns
 - Using Enums with Types

## Writing good Unit Tests
 - Mocking libraries
 - Setting expectations on Mock
 - Asserting the outcome
 - Verifying the result

## Declarative programming
- Use declarative programming over iterative - Ex: jdbTemplate.queryXXX methods
- Use Annotations for type compile time checking (@Override, @ControlAdrive, @ResponseStatus)
- Use declarative validations over imperative style (Hibernate validator)

## Bonus
- Use lombok/Kotlin for generating boiler plate code
- Enforce static checks using Checkstyle, FindBugs, PMD
- Use consistent tooling, coding quidelines
- Be productive
    - Use key board shortcuts
    - Use Live templates
    - Keep wide working area 
- Commit often to feature branches
- Use feature branches and best Git merging strategies

## Refrences
- [Functional programming patterns](https://dzone.com/articles/functional-programming-patterns-with-java-8)
- Clean Code: A Handbook of Agile Software Craftsmanship (Robert C. Martin)
- [Video Link](https://www.youtube.com/watch?v=F02LKnWJWF4&feature=youtu.be)
 


